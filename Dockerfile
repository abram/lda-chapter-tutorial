FROM ubuntu:14.04

RUN apt-get update
RUN apt-get -y install curl git  libxml-xpath-perl libxml-dom-perl libjson-perl vowpal-wabbit python  r-cran-zoo r-cran-zoo r-cran-zoo r-base-core python-nltk python-dateutil vim-nox

RUN curl -sSL https://rvm.io/mpapis.asc | gpg --import -
RUN curl -L https://get.rvm.io | bash -s stable;
ADD docker-env.sh ./
RUN ./docker-env.sh rvm install 1.9.2

RUN ./docker-env.sh gem install octokit

# RUN apt-get -y install vowpal-wabbit
RUN sudo cpan -T -f VCI

ADD . ./lda-chapter-tutorial
WORKDIR ./lda-chapter-tutorial

ENTRYPOINT ./docker-env.sh bash
